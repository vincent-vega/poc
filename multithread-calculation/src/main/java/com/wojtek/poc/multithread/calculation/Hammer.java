package com.wojtek.poc.multithread.calculation;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Wojciech Koszycki
 */
public class Hammer {

    private static final Logger LOG = Logger.getLogger(Hammer.class.getName());

    /**
     * Simply calculates average of table
     *
     * @param rocks
     * @return average of rocks
     */
    public static long calcAverageRocksCrushed(double[] rocks) {
        double sum = 0;
        for (int i = 0; i < rocks.length; i++) {
            sum += rocks[i];
        }
        return Math.round(sum / rocks.length);
    }

    /**
     * Calculates percent of rock that is crushed
     *
     * @param rockSize
     * @param power
     * @return power percent of rock
     * @throws InterruptedException
     */
    public double crashRock(int rockSize, int power) throws InterruptedException {
        LOG.log(Level.INFO, "Start counting percentage for:{0},{1}", new Object[]{rockSize, power});
        //pretend it is hard to calculate it ..
        Thread.sleep(2000);
        LOG.log(Level.INFO, "job done for:{0},{1}", new Object[]{rockSize, power});
        return rockSize * power / 100;
    }

}
