package com.wojtek.poc.multithread.calculation;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import static junit.framework.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Wojciech Koszycki
 */
public class ExecutorsTest {

    private static final Logger LOG = Logger.getLogger(ExecutorsTest.class.getName());
    private static final int[] ROCKS = new int[]{100, 200, 300, 400};
    Collection<Callable<Double>> workers;

    Hammer instance;

    @Before
    public void setUp() {
        this.instance = new Hammer();

        this.workers = new HashSet<>(4);
        for (int i = 0; i < ROCKS.length; i++) {
            this.workers.add(new Imigrant(instance, 20, ROCKS[i]));
        }
    }

    public ExecutorsTest() {
    }

    @Test
    public void fixedThreadPoolTest() throws InterruptedException, ExecutionException {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        LOG.info("Job outsourced to cheap Imigrants :-) I'm taking a nap :)");
        List<Future<Double>> jobList = executorService.invokeAll(workers, 1l, TimeUnit.MINUTES);
        Thread.sleep(9000);
        double[] percentages = new double[4];
        for (int i = 0; i < jobList.size(); i++) {
            percentages[i] = jobList.get(i).get();
        }
        long res = Hammer.calcAverageRocksCrushed(percentages);
        assertEquals(50l, res);

    }

}
