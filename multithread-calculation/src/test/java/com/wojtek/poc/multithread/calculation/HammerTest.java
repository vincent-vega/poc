package com.wojtek.poc.multithread.calculation;

import java.util.logging.Logger;
import static junit.framework.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Wojciech Koszycki
 */
public class HammerTest {

    Hammer instance;

    @Before
    public void setUp() {
        this.instance = new Hammer();
    }

    public HammerTest() {
    }

    @Test
    public void crashRockTest() throws InterruptedException {
        double res = instance.crashRock(100, 20);
        assertEquals(20d, res);
    }

    @Test
    public void calcAverageRocksCrushedTest() {
        long res = Hammer.calcAverageRocksCrushed(new double[]{3.5, 2.5, 8});
        assertEquals(5l, res);
    }

}
